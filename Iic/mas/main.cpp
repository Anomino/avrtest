#include <avr/io.h>
#include "../I2C.h"
#include "../I2C_Status.h"
bool StartI2C_2(uint8_t address)
{
	// 開始条件送出
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// 送信完了待ち
	WAIT_COMP;

	// バスの状態が、開始条件また再送開始条件かチャック
	uint8_t twi_status = TWSR & 0xf8;
	if ( (twi_status != TWI_START) && (twi_status != TWI_RESTART))
	{
		return false;
	}

	// デバイスアドレス送出
	TWDR = address;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	WAIT_COMP;

	// check value of TWI Status Register. Mask prescaler bits.
	twi_status = TWSR & 0xf8;
	if ( (twi_status != TWI_MT_SLA_ACK) && (twi_status != TWI_MR_SLA_ACK) )
	{
		return false;
	}

	return true;
}
int main()
{
	InitI2C(100000);
	DDRB = 0x01;
	DDRD = 0x80;

	while(1)
	{
		//受信主装置
		if(StartI2C_2(0xf1) == true)
		{
			if(ReceiveNack() == 0xee)
			{
				StopI2C();
				PORTD = 0x80;
			}
			else
			{
				StopI2C();
				PORTB = 0x01;
			}
		}
		else
		{
			//PORTD = 0x80;
		}
		/*
		 //送信主装置
		if(StartI2C_2(0xf0) == true)
		{
			WriteData(0xe7);
			StopI2C();
			PORTB = 0x01;
		}
		else
		{
			//PORTD = 0x80;
		}
		*/
	}
}
