/**********************************
 * AVRのI2Cペリフェラルライブラリ *
 **********************************/

#include "I2C.h"

void InitI2C(uint32_t twi_freq)
{
	uint8_t twi_pres;

	twi_freq = (F_CPU / twi_freq - 16) / 2;
	
	if(twi_freq <= 255)            { twi_pres = 0;                }
	else if((twi_freq / 4) <= 255) { twi_pres = 1; twi_freq /= 4;  }
	else if((twi_freq / 16) <= 255){ twi_pres = 2; twi_freq /= 16; }
	else if((twi_freq / 64) <= 255){ twi_pres = 3; twi_freq /= 64; }

	TWBR = (uint8_t)twi_freq;
	TWSR = twi_pres;
	TWCR |= 0x01;
}

bool StartI2C(uint8_t address)
{
	// 開始条件送出
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// 送信完了待ち
	WAIT_COMP;

	// バスの状態が、開始条件また再送開始条件かチャック
	uint8_t twi_status = TWSR & 0xf8;
	if ( (twi_status != TWI_START) && (twi_status != TWI_RESTART))
	{
		return false;
	}

	// デバイスアドレス送出
	TWDR = address;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	WAIT_COMP;

	// check value of TWI Status Register. Mask prescaler bits.
	twi_status = TWSR & 0xf8;
	if ( (twi_status != TWI_MT_SLA_ACK) && (twi_status != TWI_MR_SLA_ACK) )
	{
		return false;
	}

	return true;
}

void StopI2C()
{
    // 終了条件送出
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	
	// 終わるまで待つ
	while(TWCR & (1<<TWSTO));
}

bool WriteData(uint8_t data)
{
	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	WAIT_COMP;

	// check value of TWI Status Register. Mask prescaler bits
	uint8_t twi_status = TWSR & 0xF8;
	if((twi_status != TWI_MT_DATA_ACK) && (twi_status != TWI_ST_DATA_ACK) && (twi_status != TWI_ST_DATA_NACK))
	{
		return false;
	}
	
	return true;
}

uint8_t ReceiveAck()
{
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
	WAIT_COMP;
	
    return TWDR;
}

uint8_t ReceiveNack()
j
	TWCR = (1 << TWINT) | (1 << TWEN);
	WAIT_COMP;
	
    return TWDR;
}
