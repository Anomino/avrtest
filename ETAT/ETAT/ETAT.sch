EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA328-P IC?
U 1 1 56C8076A
P 5650 4100
F 0 "IC?" H 4900 5350 50  0000 L BNN
F 1 "ATMEGA328-P" H 6050 2700 50  0000 L BNN
F 2 "DIL28" H 5650 4100 50  0000 C CIN
F 3 "" H 5650 4100 50  0000 C CNN
	1    5650 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P?
U 1 1 56C807F7
P 1600 1350
F 0 "P?" H 1600 1500 50  0000 C CNN
F 1 "CONN_01X02" V 1700 1350 50  0000 C CNN
F 2 "" H 1600 1350 50  0000 C CNN
F 3 "" H 1600 1350 50  0000 C CNN
	1    1600 1350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
