/****************************
 * Software PWM main source *
 ****************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "SoftwarePwm.h"

void High1()
{
	PORTC |= 0x08;
}

void Low1()
{
	PORTC &= ~0x08;
}

void High2()
{
	PORTC |= 0x04;
}

void Low2()
{
	PORTC &= ~0x04;
}

void InitMain()
{
	DDRC = 0xff;
}

SoftwarePwm pwm1(20, High1, Low1), pwm2(100, High2, Low2);

ISR(TIMER0_COMPA_vect)
{
	pwm1.SetPwm();
	pwm2.SetPwm();
}

int main()
{

	InitMain();
	InitTimer(1);
	sei();

	while(1)
	{
	}

	return -1;
}
