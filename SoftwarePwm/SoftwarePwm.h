/************************
 * Software PWM for AVR *
 ************************/

#pragma once

#include <avr/io.h>
#include "SoftwarePwm_config.h"

/*************************************************************
 * Flow of Software PWM                                      *
 *                                                           *
 * 1.Call InitTimer to Initialize timer for any freqency.    *
 * 2.Instance "SoftwarePwm" class and set pwm duty.          *
 * 3.Override "SetHigh" and "SetLow" function as a function  *
 *   output High or Low each other.                          *
 * 4.Describe "void SetPwm();" in TimerHandler.              *
 * 5.Call "InitTimer" function and drive timer.              *
 * 6.Call "sei" function.                                    * 
 *                                                           *
 *************************************************************/

class SoftwarePwm
{
	private : 
		int duty, pwmCount;
		void (*SetHigh)(), (*SetLow)();

	public :
		SoftwarePwm(int duty, void ((*SetHigh)()), void ((*SetLow)()));
		void SetDuty(int duty);
		int  CheckDuty();
		void SetPwm();
};

/*******************************
 * TimerCh                     *
 * 1 : F_CPU / 8    / DUTY_MAX *
 * 2 : F_CPU / 64   / DUTY_MAX *
 * 3 : F_CPU / 256  / DUTY_MAX *
 * 4 : F_CPU / 1024 / DUTY_MAX *
 *******************************/
void InitTimer(int timerCh);
