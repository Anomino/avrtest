/******************************************
 * Configuration file for SoftwarePwm.cpp *
 ******************************************/

#ifndef _SOFTWARE_PWM_CONFIG_H_
#define _SOFTWARE_PWM_CONFIG_H_ 0

// MAXvalue for pulse count
#define DUTY_MAX   10000
// PWM MODE (Value of WGM Registor)
#define PWM_MODE   0x02
// Timer Prescalor
#define TIMER_PRES


#endif
