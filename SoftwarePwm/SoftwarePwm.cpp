/************************
 * Software PWM for AVR *
 ************************/

#include "SoftwarePwm.h"

SoftwarePwm :: SoftwarePwm(int duty, void (*SetHigh)(), void (*SetLow)())
{
	this->SetDuty(duty);
	this->SetHigh = SetHigh;
	this->SetLow  = SetLow;
}

void SoftwarePwm :: SetDuty(int duty)
{
	this->duty = duty;
}

int SoftwarePwm :: CheckDuty()
{
	return this->duty;
}

void SoftwarePwm :: SetPwm()
{
	if(this->pwmCount >= this->duty)
	{
		this->SetLow();
	}
	else
	{
		this->SetHigh();
	}

	if(this->pwmCount >= DUTY_MAX)
	{
		this->pwmCount = 0;
	}

	this->pwmCount ++;
}

void InitTimer(int presCh)
{
	//Initialize Timer0
	TCCR0A = PWM_MODE & 0x03;
	TCCR0B = ((PWM_MODE & 0x04) << 1) | (presCh + 1);
	OCR0A  = DUTY_MAX;
	TIMSK0 |= 0x02; 
}
