/*********************************************************************
 * AVRマイコンによる、タイマーを用いた擬似マルチスレッドのライブラリ *
 * 											簡易バージョン			 *
 * いずれは、listを使って整理したい。								 *
 * *******************************************************************/
#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>

/*******************************
 * タイマを用いて、ごく短い時間間隔で割り込みをかけて
 * あらかじめ用意した関数ポインタの配列を、割り込みがかかる度に進めていく。
 *
 * [注意点]
 * 割り込みがかかる時間間隔 < 関数ポインタの指す関数内の処理時間
 * になるとマズいかも。
 * ***********************************************************************/

static void (* threads[])();
static int ele_num, cur_num = 0;

ISR(TIMER0_COMPA_vect)
{
	cur_num ++;

	if(cur_num >= ele_num)
	{
		cur_num = 0;
	}
}

inline void InitMultiThread(void (*thread_list[])())
{
	//Initialize Timer0
	TCCR0A = 0x02;
	TCCR0B = 0x01;
	OCR0A  = 100;
	TIMSK0 |= 0x02; // 比較一致Aの割り込み許可

	threads 
}

inline void MultiThreadDriver()
{
	threads[cur_num]();	
}

