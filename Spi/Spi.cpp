/************************************
 * AVR用のSPIペリフェラルライブラリ *
 ************************************/

#include "Spi.h"

bool receiveCheck = false;

ISR(SPI_STC_vect)
{
	receiveCheck = true;
}

SpiSlave :: SpiSlave(void (*RiseSs)(), void (*DropSs)())
{
	this->RiseSs = RiseSs;
	this->DropSs = DropSs;

	this->RiseSs();
}

void SpiSlave :: SpiTranfer(uint8_t transmitData)
{
	SPDR = transmitData;
	this->DropSs;

	while(!receiveCheck);
	this->RiseSs();
	receiveCheck = false;
}
/*
bool SpiSlave :: SpiRecieve(uint8_t *receiveData)
{
	*receiveData = SPDR;
	return receiveCheck;
}
*/
