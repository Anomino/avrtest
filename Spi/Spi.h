/**************************************
 * AVR用のSPIペリフェラルのライブラリ *
 **************************************/

#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Spi_config.h"

inline void InitAsSpiSlave()
{
	SPCR = (0xc0 | (_DATA_ORDER_ << 5) | (_CLOCK_POLARITY_ << 3) | (_CLOCK_PHASE_ << 2));
}

inline void InitAsSpiMaster(uint8_t spiFreqCh)
{
	if(spiFreqCh > 3)
	{
		spiFreqCh = 2;
	}

	SPCR =  (0xd0 | (_DATA_ORDER_ << 5) | (_CLOCK_POLARITY_ << 3) | (_CLOCK_PHASE_ << 2) | spiFreqCh);
}

class SpiSlave
{
	private : 
		void (*RiseSs)(), (*DropSs)();
		uint8_t receiveData;

	public : 
		SpiSlave(void (*RiseSs)(), void (*DropSs)());
		void SpiTranfer(uint8_t transmitData);
		
		uint8_t SpiRecieve()
		{
			return SPDR;
		}
};
