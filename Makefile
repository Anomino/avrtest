############################
# Makefile for AVR Devices #
#          Author Kamisama #
############################

ifndef PROG
PROG = sample
endif

ifndef FREQ
FREQ = 8000000
endif

ifndef OPT
OPT = 2
endif

ifndef DEV
DEV = atmega328p
endif

CXX      = avr-gcc
CXXFLAGS = -g -O$(OPT) -mmcu=$(DEV) -D F_CPU=$(FREQ)UL -c
OPTION   = -g -O2 -mmcu=$(DEV) $(PROG).o $(OBJ) -o $(PROG)

All : $(PROG)

$(PROG).o : $(PROG).cpp
	$(CXX) $(PROG).cpp $(CXXFLAGS)

$(PROG) : $(PROG).o
	$(CXX) $(OPTION)

test:
	echo test
