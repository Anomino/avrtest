/**********************************
 * AVRのI2Cペリフェラルライブラリ *
 **********************************/

#pragma once

#include <avr.io.h>
#include <avr/interrupt.h>

void InitTwi(uint8_t twiFreq);

class TwiSlave
{
	private :
		 

	public  : 
		TwiSlave();
		void TwiStart();
		void TwiTransfer();
		
		void TwiStop;
};
