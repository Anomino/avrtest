# coding : UTF-8

#################################
# Make and Send for AVR ver 2.0 #
#################################

# Constants
DEF_FREQ   = "8000000UL "
DEF_DEV    = "atmega328p "
DEF_OPTLEV = "2 "

import os

def MakeHex(fileName) : 
    # Make Hex from object file
    print ("avr-objcopy -O ihex " + fileName + ' ' + fileName + '.ihex')
    print ("\nLog\n")
    hexError = os.system("avr-objcopy -O ihex " + fileName + ' ' + fileName + '.ihex')
    
    if hexError == 0 : 
        os.system("rm " + fileName)

    else : 
        # Error Process
        pass

def TransferFile(fileName, flash = "flash", dev = "m328p") : 
    # Transfer to AVR
    print ("sudo avrdude -c avrispmkII -P usb -p " + dev + " -U" + flash + ":w:" + fileName)
    print ("\nLog\n")
    transError = os.system("sudo avrdude -c avrispmkII -P usb -p " + dev + " -U " + flash + ":w:" + fileName)

def Menu() : 
    # Menu mode
    os.system("clear")

    print ("########################")
    print ("# Make and Send to AVR #")
    print ("#          Menu mode   #")
    print ("########################\n\n\n")

    print ("########################")
    print ("# Mode     ## Commands #")
    print ("#----------##----------#")
    print ("# Compile  ## 0        #")
    print ("# MainCom  ## 1        #")
    print ("# Transfer ## 2        #")
    print ("# Show     ## 3        #")
    print ("# Help     ## 4        #")
    print ("# Quit     ## 5        #")
    print ("########################\n")

    print ("Choose the mode command. : ", end = '')
    mode = input()
    if mode == '0' : 
        CompileMode()

    elif mode == '1' : 
        MainCompileMode()

    elif mode == '2' : 
        TransferMode()

    elif mode == '3' : 
        ShowMode()

    elif mode == '4' : 
        Helpmode()

    else : 
        print ("Error!")

def CompileMode() : 
    # Compile mode
    os.system("clear")

    print ("########################")
    print ("# Make and Send to AVR #")
    print ("#       Compile mode   #")
    print ("########################\n\n\n")

    print ("Input a C++ file name.(No extention) : ", end = '')
    fileName = input()
    print ("Default compile? Y/N : ", end = '')
    ans = input()
    if ans == 'y' or ans == 'Y' : 
        # default compile
        print ("avr-gcc -g -O" + DEF_OPTLEV + 
        "-mmcu=" + DEF_DEV + "-D F_CPU=" + DEF_FREQ + "-c " + fileName + ".cpp")
        print ("\nLog\n")
        compileError = os.system("avr-gcc -g -O" + DEF_OPTLEV + 
        "-mmcu=" + DEF_DEV + "-D F_CPU=" + DEF_FREQ + "-c " + fileName + ".cpp" )
        
        print ("\navr-gcc -g -O" + DEF_OPTLEV + "-mmcu=" + DEF_DEV + fileName + ".o -o " + fileName)
        print ("\nLog\n")
        os.system("avr-gcc -g -O" + DEF_OPTLEV + "-mmcu=" + DEF_DEV + fileName + ".o -o " + fileName)

    else : 
        # custom compile
        pass

def MainCompileMode() : 
    # Make main mode
    os.system("clear")

    print ("########################")
    print ("# Make and Send to AVR #")
    print ("#  Main Compile mode   #")
    print ("########################\n\n\n")

    print ("Input a C++ file name.(No extention) : ", end = '')
    fileName = input()
    print ("Object file(with Extention) : ", end = '')
    objName = input()
    print ("Default compile? Y/N : ", end = '')
    ans = input()
    if ans == 'y' or ans == 'Y' : 
        # default compile
        print ("avr-gcc -g -O" + DEF_OPTLEV + 
        "-mmcu=" + DEF_DEV + "-D F_CPU=" + DEF_FREQ + "-c " + fileName + ".cpp ")
        print ("\nLog\n")
        compileError = os.system("avr-gcc -g -O" + DEF_OPTLEV + 
        "-mmcu=" + DEF_DEV + "-D F_CPU=" + DEF_FREQ + "-c " + fileName + ".cpp ")
        if compileError == 0 : 
            print ("\navr-gcc -g -O" + DEF_OPTLEV + "-mmcu=" + DEF_DEV + objName + ' ' + fileName + ".o -o " + fileName)
            print ("\nLog\n")
            os.system("avr-gcc -g -O" + DEF_OPTLEV + "-mmcu=" + DEF_DEV + objName + ' ' + fileName + ".o -o " + fileName)
        
        else : 
            # Error Process
            return -1

    else : 
        # custom compile
        pass

    MakeHex(fileName)

def ShowMode() : 
    os.system("clear")

    print ("########################")
    print ("# Make and Send to AVR #")
    print ("#      Show mode       #")
    print ("########################\n\n\n")

    os.system("ls -la")

def Helpmode() : 
    # Help mode
    pass

def TransferMode() : 
    # Transfer mode
    os.system("clear")

    print ("########################")
    print ("# Make and Send to AVR #")
    print ("#      Transfer mode   #")
    print ("########################\n\n\n")

    print ("Transfer ihex data(No extention) : ", end = '')
    fileName = input()

    print ("Default Transfer Y/N : ", end = '')
    ans = input()
    if ans == 'y' or ans == 'Y' :
        TransferFile(fileName)

    else : 
        pass

def EndProcess() : 
    # End Process
    print ("Thank you for using this python tool!")

if __name__ == "__main__" : 
    continueFlag = True

    while continueFlag == True : 
        Menu()

        print ("Would you continue? Y/N : ", end = '')
        ans = input()
        if ans == 'y' or ans == 'Y' : 
            pass
        else : 
            continueFlag = False

    EndProcess()
