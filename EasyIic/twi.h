/*********************************
*   TWIを用いたI2Cのライブラリ   *
*********************************/

/* 使用する際には、SCLピンとSDAピンにプルアップ抵抗をはさむのを忘れずに！ */

#ifndef  _I2C_set_
#define _I2C_set_

#include <avr/io.h>

#define RE 0x01
#define WR 0x02

void init_twi(uint32_t);      //TWIの初期設定 (SCL周波数)
void start_twi(void);         //TWIスタート
void stop_twi(void);          //TWIストップ
void transmit_twi(uint8_t);   //データを送信 (送信データ)
uint8_t receive_twi(void);    //データ受信

#endif
