#ifndef _use_ACM1602NI_FLW_FBW_M01_
#define _use_ACM1602NI_FLW_FBW_M01_

#include <avr/io.h>
#include "twi.h"
#include <util/delay.h>
#include <stdio.h>

#define LCD_CLEAR 0x01
#define LCD_AHEAD_CURSOR 0x02

/* 先にTWI機能を初期化しておくこと */

void init_ACM1602NI_FLW_FBW_M01(void);                  //LCDの初期化
void write_to_ACM1602NI_FLW_FBW_M01(uint8_t,uint8_t);   //LCDへ書き込み(デバイスアドレス)
void print_ACM1602NI_FLW_FBW_M01(uint8_t);              //LCDへ書き込み(表示用のアドレス)
void change_mode(uint8_t);                              //モード選択


#endif
