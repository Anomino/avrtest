#include "twi.h"

uint32_t ReceiveAcce()
{
	uint32_t data;

	start_twi();
	transmit_twi(0x1D | 0x00);
	transmit_twi(0x32);
	start_twi();
	transmit_twi(0x1D | 0x01);
	data = receive_twi();
	stop_twi();
}

int main()
{
	init_twi(1000000);
	DDRC = 0x0c;
	uint32_t data[2];

	while(1)
	{
		data[0] = ReceiveAcce();
		data[1] = ReceiveAcce();

		if(data[0] != data[1])
		{
			PORTC = 0x08;
		}
		else
		{
			PORTC = 0x04;
		}
	}
}
