#include "twi.h"

void init_twi(uint32_t freq){
	uint32_t twi_freq = 0;
	uint8_t pres = 0;
	
	twi_freq = ((uint32_t)F_CPU / (uint32_t)freq - 16) / 2;
	if     (twi_freq <= 255)       { pres = 0; }
	else if((twi_freq / 4) <= 255) { pres = 1; twi_freq /= 4; }
	else if((twi_freq / 16) <= 255){ pres = 2; twi_freq /= 16; }
	else if((twi_freq / 64) <= 255){ pres = 3; twi_freq /= 64; }
	
	TWBR = (uint8_t)twi_freq;
	TWSR = pres;
}

void start_twi(void){
	//printf("START_TWI\n");
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
	loop_until_bit_is_set(TWCR,TWINT);
}

void stop_twi(void){
	TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
}

void transmit_twi(uint8_t t_data){
	TWDR = t_data;
	TWCR = _BV(TWINT) | _BV(TWEN);
	loop_until_bit_is_set(TWCR,TWINT);
}

uint8_t receive_twi(void){
	uint8_t r_data = 0;
	
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWEA);
	loop_until_bit_is_set(TWCR,TWINT);
	r_data = TWDR;
	
	return r_data;
}
