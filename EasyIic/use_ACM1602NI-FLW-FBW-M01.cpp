#include "use_ACM1602NI-FLW-FBW-M01.h"

void init_ACM1602NI_FLW_FBW_M01(void){
	_delay_ms(15);
	write_to_ACM1602NI_FLW_FBW_M01(0x00,0x01);
	_delay_ms(5);
	write_to_ACM1602NI_FLW_FBW_M01(0x00,0x38);
	_delay_ms(5);
	write_to_ACM1602NI_FLW_FBW_M01(0x00,0x0c);
	_delay_ms(5);
	write_to_ACM1602NI_FLW_FBW_M01(0x00,0x06);
	_delay_ms(5);
}

void write_to_ACM1602NI_FLW_FBW_M01(uint8_t add,uint8_t t_data){
	start_twi();
	transmit_twi(0xa0);
	transmit_twi(add);
	transmit_twi(t_data);
	stop_twi();
}

void print_ACM1602NI_FLW_FBW_M01(uint8_t t_data){ write_to_ACM1602NI_FLW_FBW_M01(0x80,t_data); }
void change_mode                (uint8_t mode)  { write_to_ACM1602NI_FLW_FBW_M01(0,mode); }
	
