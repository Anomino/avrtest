/**********************************
 * AVRのADCペリフェラルライブラリ *
 **********************************/

//#pragmaディレクティブによるインクルードガード
#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Adc_config.h"

//ADCペリフェラルの初期化
void InitAdc();

//アナログデータのリスナーオブジェクト
class AnalogListener
{
	private :
		uint8_t adcData, myAdcCh;

	public : 
		//コンストラクタのインライン化
		AnalogListener(uint8_t adcCh)
		{
			if(adcCh > 8)
			{
				this->myAdcCh = 0;
			}
			else
			{
				this->myAdcCh = adcCh;
			}
		}
		
		//ADCを開始もインライン化
		void StartAdc()
		{
			ADMUX  |= (0xe0 | ~myAdcCh);
			ADCSRA |= 0x40;
		}

		uint16_t AdReceive();
};
