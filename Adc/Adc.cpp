/**********************************
 * AVRのADCペリフェラルライブラリ *
 **********************************/

#include "Adc.h"

uint16_t adData = 0;

ISR(ADC_vect)
{
	adData = ADCH | ADCL;
}

void InitAdc()
{
	ADMUX  |= (_REF_VOL_ << 6);
	ADCSRA |= 0x88 | (_ADC_PRS_);
}

uint16_t AdReceive()
{
	return adData;
}
