/**********************************
 * UARTペリフェラル用のライブラリ *
 *   author Yukiya 2016 / 1 /16   * 
**********************************/

#include "Uart.h"

//外部リンケージ用の変数 UART通信で扱うデータが格納される。
volatile uint8_t uartTxData, uartRxData;
volatile bool uartCheckFlag = false;

//割り込みハンドラ
ISR(USART_RX_vect)
{
	uartRxData = UDR0;
}

ISR(USART_UDRE_vect)
{
	UDR0 = uartTxData;
}

ISR(USART_TX_vect)
{
	uartCheckFlag = true;
}

//UARTペリフェラルの初期化
void InitUart(int baud)
{
	baud = (F_CPU / 16 / baud - 1);
	UBRR0  = baud;
	
	UCSR0B = 0xf8;
	UCSR0C = 0x06;
}
