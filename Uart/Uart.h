/**********************************
 * UARTペリフェラル用のライブラリ *
 *  author Yukiya 2016 / 1 /21    *
 **********************************/

//#pragmaディレクティブによる、インクルードガード
#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>

void InitUart(int baud);
